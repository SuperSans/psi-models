#include <iostream>
#include <vector>
#include <ctime>
using namespace std;
#define exc_A         1.0 /* ESTIMATE */
#define inh_A         1.0 /* ESTIMATE */
#define tau_pp        1.5 /* ESTIMATE */
#define tau_pb        1.5 /* ESTIMATE */
#define tau_bp        3.3
#define tau_bb        1.2
#define E_inh         -0.67
#define E_exc         4.67
#define g_l           .05
/*This class will provide all functionalities of a neuron in CA3 */

class Neuron{
  /*Define attributes and functionalities of neuron here*/
  public:
    double membrane_potential = 0.0, prev_membrane_potential;
    double background_current, prev_background_current;
    double exc_conductance, prev_exc_conductance;
    double inh_conductance, prev_inh_conductance;
    double time_fired = -1.0;
    vector<int> pyramidal_connections;
    vector<int> inhibitory_connections;
  
    Neuron(double meta_probability, double ext_probability, int type, int pyramidal_count, int inhibitory_count){
      /* Constructor used for both inhibitory and excitatory. Meta probability
       * is the probability of b->b or p->p connection, External probability is
       * probability of b->p or p->b connection. Type determines if pyramidal
       * or interneuron */
      
      int num_pyramidal_connections = (int)(ext_probability * (float) pyramidal_count);
      int num_interneuron_connections = (int)(meta_probability * (float) inhibitory_count);
      srand(time(NULL));    /* Set a varying seed for each neuron */
      for (int i = 0; i < num_pyramidal_connections; i++){
        pyramidal_connections.push_back(rand() % pyramidal_count);  //This method produces repeats. Not good.
      }
      for (int j = 0; j < num_pyramidal_connections; j++){
        inhibitory_connections.push_back(rand() % inhibitory_count);
      }
    };
  
    void CalcExcConductance(vector<Neuron> excitatory_neurons, double time, int type){
      /* Calculate the conductance g_exc based on summation of synapsed neurons
       * whose indices are found in pyramidal_connections */
      double exc_sum = 0.0;
      double time_diff;
      
      for (int i = 0; i < pyramidal_connections.size(); i++){
        Neuron neuron = excitatory_neurons[pyramidal_connections[i]];
        if (type == 0){   /* Excitatory -> Inhibitory */
          if (neuron.time_fired != -1.0) exc_sum += exc_A * exp(-(time - neuron.time_fired)/ tau_pb);
        } else if (type == 1){   /* Excitatory -> Excitatory */
          if (neuron.time_fired != -1.0) exc_sum += exc_A * exp(-(time - neuron.time_fired)/ tau_pp);  /*Ae^{-(t-t*)/T} */
        }
        prev_exc_conductance = exc_conductance;
        exc_conductance = exc_sum;
      }
    };
  
    void CalcInhConductance(vector<Neuron> inhibitory_neurons, double time, int type){
      /* Calculate the conductance g_inh based on summation of synapsed neurons
       * wbose indices are found in inhibitory_connections */
      double inh_sum = 0.0;
      double time_diff;
      
      for (int i = 0; i < pyramidal_connections.size(); i++){
        Neuron neuron = inhibitory_neurons[pyramidal_connections[i]];
        if (type == 0){   /* Inhibitory -> Inhibitory */
          if (neuron.time_fired != -1.0) inh_sum += inh_A * exp(-(time - neuron.time_fired)/ tau_bb);
        } else if (type == 1){   /* Inhibitory -> Excitatory */
          if (neuron.time_fired != -1.0) inh_sum += inh_A * exp(-(time - neuron.time_fired)/ tau_bp);  /*Ae^{-(t-t*)/T} */
        }
        prev_inh_conductance = inh_conductance;
        inh_conductance = inh_sum;
      }
    };
  
    void CalcBackgroundCurrent(){ /* Generate better noisy current */
      prev_background_current = background_current;
      background_current = ((double) rand() / (RAND_MAX)) * 2.0;
    };
  
    double CalcMembranePotential(double time){ /*Euler's Method on dv/dt = g_L * V - g_inh(t)[V-E_inh] - g_exc(t)[V-E_exc] + I_bg */
      prev_membrane_potential = membrane_potential;
      double dvdt = g_l * prev_membrane_potential - 
        inh_conductance * (prev_membrane_potential - E_inh) - 
        exc_conductance * (prev_membrane_potential - E_exc) +
        background_current;
      membrane_potential = dvdt;
      return membrane_potential;
    };
  
    void fire(double time){
      prev_membrane_potential = 0;
      membrane_potential = 0;
      time_fired = time;
    };
};