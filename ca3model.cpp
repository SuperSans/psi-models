#include <iostream>
#include "neuron.cpp"
#include <vector>
using namespace std;

const int interneuron_count = 400;
const int pyramidal_count   =  80;
const double pp_probability = .05;
const double pb_probability = .45;
const double bb_probability = .01;
const double bp_probability = .90;
const double TIME_STEP      = .10;
const double TOTAL_TIME     = 1.00;
double current_time         = 0.0;

vector<Neuron> inhibitory_neurons;
vector<Neuron> excitatory_neurons;
vector<double> spikes;

int main(){  
  std::cout << "Model running.\n";
  for (int i = 0; i < interneuron_count; i++){
    Neuron neuron = Neuron(bb_probability, bp_probability, 0, pyramidal_count, interneuron_count);
    inhibitory_neurons.push_back(neuron);
  }
  
  for (int j = 0; j < pyramidal_count; j++){
    Neuron neuron = Neuron(pp_probability, pb_probability, 1, pyramidal_count, interneuron_count);
    excitatory_neurons.push_back(neuron);
  }
  
  while (current_time < TOTAL_TIME){     /* Calculate steps for each neuron here */
    
    /* Calculate neuron conductances */
    for (int i = 0; i < excitatory_neurons.size(); i++){
      Neuron neuron = excitatory_neurons[i];
      neuron.CalcExcConductance(excitatory_neurons, current_time, 1);
    }
    for (int i = 0; i < inhibitory_neurons.size(); i++){
      Neuron neuron = inhibitory_neurons[i];
      neuron.CalcInhConductance(inhibitory_neurons, current_time, 0);
    }
    
    /* Calculate background current */
    for (int i = 0; i < excitatory_neurons.size(); i++){
      Neuron neuron = excitatory_neurons[i];
      neuron.CalcBackgroundCurrent();
      if (i == 0)printf("%.2f\n", neuron.background_current);
    }
    for (int i = 0; i < inhibitory_neurons.size(); i++){
      Neuron neuron = inhibitory_neurons[i];
      neuron.CalcBackgroundCurrent();
    }
    
    /* Calculate membrane potentials, check for spikes */
    double membrane_potential;
    for (int i = 0; i < excitatory_neurons.size(); i++){
      Neuron neuron = excitatory_neurons[i];
      membrane_potential = neuron.CalcMembranePotential(current_time);
      if (membrane_potential >= 1.0){
        printf("SPIKE\n");
        neuron.fire(current_time);
        spikes.push_back(current_time);
      }
      //if (i == 0) printf("%.4f\n", membrane_potential);
    }
    for (int i = 0; i < inhibitory_neurons.size(); i++){
      Neuron neuron = inhibitory_neurons[i];
      membrane_potential = neuron.CalcMembranePotential(current_time);
      if (membrane_potential >= 1.0){
        printf("SPIKE\n");
        neuron.fire(current_time);
        spikes.push_back(current_time);
      }
    }
    current_time += TIME_STEP;            
  }
  
  for (int i = 0; i < 10; i++){
    Neuron neuron = excitatory_neurons[i];
    printf("%.2f\n", neuron.background_current);
  }
  return 0;
}